from django.contrib import admin
from .models import ExpenseCategory, Account, Receipt


class ExpenseCategoryAdmin(admin.ModelAdmin):
    list_display= ["name","owner"]
admin.site.register(ExpenseCategory)

class AccountAdmin(admin.ModelAdmin):
    list_display=["name","number","owner"]

admin.site.register(Account)

class ReceiptAdmin(admin.ModelAdmin):
    list_display = ["vendor","total","tax","date","purchaser","account"]

admin.site.register(Receipt)

# Register your models here.
