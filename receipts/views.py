from django.shortcuts import render, redirect

# Create your views here.
from .models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from .forms import CreateReceiptsForm, ExpenseCategoryForm, AccountForm


@login_required
def home(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    context = {"receipts": receipts}
    return render(request, "receipt_list.html", context)


def create_receipts(request):
    if request.method == "POST":
        form = CreateReceiptsForm(request.POST)
        if form.is_valid():
            receipt = form.save(commit=False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = CreateReceiptsForm()
    return render(request, "create_receipts.html", {"form": form})


def category_list(request):
    category = ExpenseCategory.objects.filter(owner=request.user)
    context = {"category_list": category}
    return render(request, "category_list.html", context)


def account_list(request):
    account = Account.objects.filter(owner=request.user)
    context = {"account_list": account}
    return render(request, "account.html", context)


def create_category(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            expense_category = form.save(commit=False)
            expense_category.owner = request.user
            expense_category.save()
            return redirect("category_list")
    else:
        form = ExpenseCategoryForm()
    return render(request, "create_category.html", {"form": form})


def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            expense_Account = form.save(commit=False)
            expense_Account.owner = request.user
            expense_Account.save()
            return redirect("account_list")  # no idea
    else:
        form = AccountForm()
    return render(request, "create_account.html", {"form": form})
