from django import forms #django 自带的 #User也是自带的
from receipts.models import ExpenseCategory, Account, Receipt

#@admin.register(ExpenseCategory)
class ExpenseCategoryForm(forms.ModelForm):
    class Meta: 
        model = ExpenseCategory
        fields = ["name"]

#@admin.register(Account)
class AccountForm(forms.ModelForm):
    class Meta: 
        model=Account
        fields=["name","number"]

#@admin.register(Receipt)
class CreateReceiptsForm(forms.ModelForm):
    class Meta:
        model=Receipt
        fields= ["vendor", "total", "tax", "date","category","account"]

