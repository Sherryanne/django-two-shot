from django.urls import path
from .views import (
    home,
    create_receipts,
    create_category,
    create_account,
    category_list,
    account_list,
)

urlpatterns = [
    path("", home, name="home"),
    path("create/", create_receipts, name="create_receipt"),
    path("categories/create/", create_category, name="create_category"),
    path("accounts/create/", create_account, name="create_account"),
    path("categories/", category_list, name="category_list"),
    path("accounts/", account_list, name="account_list"),
]